//
// Created by steve on 23/05/2021.
//

#include "AppWindow.h"

AppWindow::AppWindow() = default;

void AppWindow::onCreate() {

}

void AppWindow::onUpdate() {

}

void AppWindow::onDestroy() {
    Window::onDestroy();
}

AppWindow::~AppWindow() = default;
