//
// Created by steve on 23/05/2021.
//

#ifndef GRAVITYENGINE_APPWINDOW_H
#define GRAVITYENGINE_APPWINDOW_H
#pragma once
#include "../Engine/Window.h"


class AppWindow: public Window {
public:
    AppWindow();
    ~AppWindow();

    void onCreate() override;

    void onUpdate() override;

    void onDestroy() override;
};


#endif //GRAVITYENGINE_APPWINDOW_H
