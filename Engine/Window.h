//
// Created by steve on 23/05/2021.
//

#ifndef GRAVITYENGINE_WINDOW_H
#define GRAVITYENGINE_WINDOW_H
#pragma once
#include "Windows.h"

class Window {
public:
    Window();
    //Initialize the window
    bool init();
    static bool broadcast();
    //Release the window
    bool release();
    bool isRun() const;

    //EVENTS
    virtual void onCreate() = 0;
    virtual void onUpdate() = 0;
    virtual void onDestroy();

    ~Window();

protected:
    HWND m_hwnd{};
    bool m_is_run{};
};


#endif //GRAVITYENGINE_WINDOW_H
