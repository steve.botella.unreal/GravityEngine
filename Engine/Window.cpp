//
// Created by steve on 23/05/2021.
//

#include "Window.h"

Window* window = nullptr;

Window::Window() = default;

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {
    switch (msg) {
        case WM_CREATE: {
            //Event fired when the event will be created
            window->onCreate();
            break;
        }

        case WM_DESTROY: {
            //Event fired when the event will be destroyed
            window->onDestroy();
            ::PostQuitMessage(0);
            break;
        }

        default:
            return ::DefWindowProcA(hwnd, msg,wparam, lparam);
    }
    return NULL;
}

bool Window::init() {
    WNDCLASSEX wc;
    wc.cbClsExtra = NULL;
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.hbrBackground=(HBRUSH)COLOR_WINDOW;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
    wc.hInstance = NULL;
    wc.lpszClassName= "MyWindoClass";
    wc.lpszMenuName= "";
    wc.style = NULL;
    wc.lpfnWndProc = &WndProc;

    if (!::RegisterClassExA(&wc))
        return false;

    //Create the window
    m_hwnd=::CreateWindowExA(WS_EX_OVERLAPPEDWINDOW,"MyWindoClass", "DirectX Application", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 1024, 768,
                      NULL,NULL,NULL,NULL);

    //If the creation fail return false
    if (m_hwnd)
        return false;

    //Show up the window
    ::ShowWindow(m_hwnd, SW_SHOW);
    ::UpdateWindow(m_hwnd);

    if (!window)
        window = this;

    //Set this flag to indicate that the window is initialized and running
    m_is_run = true;
    return true;
}

bool Window::release() {

    //Destroy the window
    if (!::DestroyWindow(m_hwnd))
        return false;

    return true;
}

Window::~Window() = default;

bool Window::broadcast() {
    MSG msg;

    while (::PeekMessageA(&msg, NULL, 0, 0, PM_REMOVE) > 0) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    window->onUpdate();

    Sleep(0);

    return true;
}

bool Window::isRun() const {
    return m_is_run;
}

void Window::onDestroy() {
    m_is_run = false;
}
